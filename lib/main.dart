import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(FirstApp());

class FirstApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FirstAppState();
  }
}

class _FirstAppState extends State<FirstApp> {
  static const _question = [
    {
      "questionText": "Whos your girl ?",
      "answer": [
        {"text": "Puffy", "score": 1000},
        {"text": "Barbara", "score": 999},
        {"text": "poetripine?", "score": 0}
      ],
    },
    {
      "questionText": "what is your car ?",
      "answer": [
        {"text": "Range Rover Sport", "score": 2000},
        {"text": "E300 Coupe AMG Line", "score": 1000},
        {"text": "MClarren 720s", "score": 2500}
      ]
    },
    {
      "questionText": "whats your favorite board game",
      "answer": [
        {"text": "Poker", "score": 500},
        {"text": "Go fish", "score": 50},
        {"text": "Baccarat", "score": 350}
      ]
    },
  ];

  var questionIndex = 0;
  var _totalScore = 0;

  void _answerQuestion(int score) {
    if (questionIndex < _question.length) {
      print("hmmm nanya mulu nih kepo luuu");
    } else {
      print("udhhhh bawel nanya muluuu");
    }

    _totalScore += score;
    setState(() {
      questionIndex = questionIndex + 1;
    });
    print(questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("First App hehe :)"),
        ),
        body: questionIndex < _question.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: questionIndex,
                question: _question,
              )
            : Result(),
      ),
    );
  }
}
