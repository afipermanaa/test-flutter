 import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  final String
      questionText; // final itu ngejadiin kalo string ini gaakan berubah2 gt

  Question(this.questionText);
 
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,// can take as much size and width of the device
      margin: EdgeInsets.all(10),
      child: Text(
        questionText,
        style: TextStyle(fontSize: 28),
        textAlign: TextAlign.center,
      ),
    );
  }
}
